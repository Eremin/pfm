app.templated_data.brigades = [
    {
        name: "Бригада №1", 
        members: ["012", "017", "018", "024", "032", "059", "064", "104", "171", "172", "265", "273", "296", "438", "439", "440", "456", "489", "499"]
    },
    {
        name: "Испытатели", 
        members: ["029", "053", "083", "170", "214", "494", "498"]
    },
    {
        name: "Участок №2 | бригада №1",
        members: ["008", "216", "450"]
    },
    {
        name: "Участок №2 | бригада №2",
        members: ["110", "491", "403"]
    },
    {
        name: "Участок №3 | бригада №1",
        members: ["087", "148"]
    },
    {
        name: "Участок №3 | бригада №2",
        members: ["025", "263", "286", "317", "011", "315"]
    },
    {
        name: "Участок №3 | бригада №3",
        members: ["311", "303", "174", "305", "447"]
    },
    {
        name: "Участок №3 | бригада №4",
        members: ["375", "316", "044"]
    },
    {
        name: "Участок №3 | бригада №5",
        members: ["013", "444", "048", "153", "261", "492", "321", "318"]
    },
    {
        name: "Травильщики", 
        members: ["075", "163", "270", "306", "308", "313", "322", "398", "495"]
    },
    {
        name: "Участок №4 | бригада №1",
        members: ["079", "417", "097", "457"]
    },
    {
        name: "Уч.№4 | Беляев и Гигирев",
        members: ["402", "176"]
    }
]