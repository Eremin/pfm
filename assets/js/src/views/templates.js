// Объект шаблонов
app.views.templates = {
    area: _.template(app.views.elements.templates.$area.html()),
    brigade: _.template(app.views.elements.templates.$brigade.html()),
    brigade_card: _.template(app.views.elements.templates.$brigade_card.html()),
    outfit: _.template(app.views.elements.templates.$outfit.html()),
    worker: _.template(app.views.elements.templates.$worker.html()),
    editable_worker: _.template(app.views.elements.templates.$editable_worker.html()),
    worker_selector_el: _.template(app.views.elements.templates.$worker_selector_el.html()),
    brigade_selector_el: _.template(app.views.elements.templates.$brigade_selector_el.html()),
    menu_selector_el: _.template(app.views.elements.templates.$menu_selector_el.html()),
    brigades_results_template: _.template(app.views.elements.templates.$brigades_results_template.html()),
    area_results_template: _.template(app.views.elements.templates.$area_results_template.html()),
    brigade_card_results_template: _.template(app.views.elements.templates.$brigade_card_results_template.html()),
    results: _.template(app.views.elements.templates.$results.html())
}