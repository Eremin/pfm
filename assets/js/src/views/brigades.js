// Объект представлений для бригад
app.views.brigades = {};
// Список представлений для бригад
app.views.brigades.list = [];

/**
 * Рендерит бригаду
 * @param data - данные для рендера
 * @returns html
*/
app.views.brigades.renderBrigade = function (data) {
    return app.views.templates.brigade(data);
}

/**
 * Рендерит карточку бригады
 * @param data - данные для рендера
 * @return html
 */
app.views.brigades.renderBrigadeCard = function (data) {
    return app.views.templates.brigade_card(data);
}

/**
 * Кеширует представление бригады и отображает его на странице
 * @param html - представление участка
 */
app.views.brigades.showBrigade = function (target, html) {
    var $el = $(html),
        $target = $(target);

    app.views.brigades.list.push($el);
    $target.before($el);
}

/**
 * Удаляет представление по номеру бригады
 * @param id - номер бригады
 */
app.views.brigades.deleteBrigade = function (id) {
    var brigades = _.remove(app.views.brigades.list, function ($brigade) {
        return $brigade.data("id") === id;
    });

    _.forEach(brigades, function ($brigade) {
        $brigade.remove();
    });
}

/**
 * Обновляет представление бригады по ее номеру
 * @param id - номер бригады
 */
app.views.brigades.updateBrigade = function (id) {
    var brigades = _.remove(app.views.brigades.list, function ($brigade) {
        return $brigade.data("id") === id;
    }),
        data = app.brigades.getBrigadeById(id),
        area = app.areas.getAreaByBrigadeId(id);

    _.forEach(brigades, function ($brigade) {
        var $html = $(app.views.brigades.renderBrigade(data));

        app.views.brigades.list.push($html);
        $brigade.before($html);

        $brigade.remove();
    });

    // Также обновляет участок (если поменялась бригада, то должен поменяться участок)
    app.views.areas.updateAreaTotalById(area.area_id);
}

/**
 * Обновляет результаты карточки бригады
 * @param id - номер бригады
 */
app.views.brigades.updateBrigadeCardResult = function (id) {
    var data = app.brigades.getBrigadeById(id),
        $html = $(app.views.templates.brigade_card_results_template(data)),
        $results = app.views.elements.popup.$el.find(
            app.views.selectors.brigades.results
        );

    $results.before($html);
    $results.remove();
}