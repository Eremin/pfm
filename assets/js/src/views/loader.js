// Объект загрузчика
app.views.loader = {}

/**
 * Загружает и отображает сохраненные данные
 */
app.views.loader.load = function () {
    var data = app.keeper.getData(), // Получаем сохраненные данные
        // Кешируем кнопку добавления нового участка
        $areas_add = app.views.elements.$body.find(
            app.views.selectors.areas.add
        );

    // Если версия приложения отличается от сохраненной,
    // то удаляем данные и перезагружаем страницу
    if (data.version != app.data.version) {
        app.keeper.clear();
        window.location.reload();
    }

    // Устанавливаем дату из сохраненных данных
    app.data.date = new Date(data.date);

    // Проходимся по всем участкам сохраненных данных
    _.forEach(data.areas, function (area) {
        var list_len,
            $area,
            $area_name,
            $brigade_add;

        // Добавляет новый участок 
        $areas_add.trigger('click');

        // Кешируем этот новый участок
        list_len = app.views.areas.list.length;
        $area = app.views.areas.list[list_len - 1];

        // Устанавливаем его название из сохраненных данных
        $area_name = $area.find(app.views.selectors.areas.name);
        $area_name.html(area.name);
        $area_name.trigger('input');

        // Проходимся по бригадам участка
        _.forEach(area.brigades, function (saved_brigade) {
            var brigade,
                $brigade;

            // Добавляет новую бригаду
            $brigade_add = $area.find(
                app.views.selectors.brigades.add
            );
            $brigade_add.trigger("click");

            // Кешируем эту новую бригаду
            list_len = app.views.brigades.list.length;
            $brigade = app.views.brigades.list[list_len - 1];

            // Получаем бригаду по ее номеру
            brigade = app.brigades.getBrigadeById(
                $brigade.data('id')
            );

            // Меняем данные бригады
            app.loader.replaceBrigadeData(
                brigade, saved_brigade
            );
            // Обновляем представление бригады
            app.views.brigades.updateBrigade(
                $brigade.data('id')
            );
        });

        // Производим вычисление результатов
        app.calculator.calculateAllAreas();
        app.views.areas.updateAreaTotalById(
            $area.data('id')
        );
    });

    app.keeper.save();
    app.loader.updateCurrentIDs();
    // Производим вычисление результатов
    app.calculator.calculateAllAreas();
    app.views.body.updateResults();
}
