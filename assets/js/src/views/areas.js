// Объект представлений для участков
app.views.areas = {};
// Список представлений для участков
app.views.areas.list = [];

/**
 * Рендерит участок
 * @param data - данные для рендера
 * @returns html
*/
app.views.areas.renderArea = function (data) {
    return app.views.templates.area(data);
}

/**
 * Кеширует представление и отображает его на странице
 * @param html - представление участка
 */
app.views.areas.showArea = function (html) {
    var $el = $(html);

    app.views.areas.list.push($el);
    app.views.elements.areas.$container.append($el);
}

/**
 * Удаляет представление по номеру участка
 * @param id - номер участка
 */
app.views.areas.deleteArea = function (id) {
    var areas = _.remove(app.views.areas.list, function ($area) {
        return $area.data("id") === id;
    });

    _.forEach(areas, function ($area) {
        $area.remove();
    });
}

/**
 * Обновляет total участка по id
 * @param id - номер участка 
 */
app.views.areas.updateAreaTotalById = function (id) {
    var $area = _.find(app.views.areas.list, function ($area) {
        return $area.data("id") === id;
    }),
        data = app.areas.getAreaById(id),
        
    $html = $(app.views.templates.area_results_template(data)),
    $results = $area.find(app.views.selectors.areas.results);

    $results.before($html);
    $results.remove();

    // Также обновляет общие результаты (если поменялся участок, то должен поменяться общий итог)
    app.views.body.updateResults();
}

