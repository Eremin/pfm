// Объект представления результатов
app.views.result = {}

/**
 * Рендерит результат
 * @param data - данные для рендера
 * @returns html
 */
app.views.result.render = function (data) {
    return app.views.templates.results(data);
};