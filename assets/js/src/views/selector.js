// Объект представления селектора
app.views.selector = {}
app.views.selector.initiator = null;
app.views.selector.is_visible = false;

/**
 * Отображает селектор
 */
app.views.selector.show = function (initiator) {
    app.views.elements.selector.$el.show();
    app.views.selector.setPosition(initiator);
    app.views.selector.setInitiator(initiator);
    app.views.selector.is_visible = true;
}

/**
 * Устанавливает инициатор селектора
 * @param initiator - элемент вызвавший селектор
 */
app.views.selector.setInitiator = function (initiator) {
    app.views.selector.initiator = initiator;
}

/**
 * Скрывает селектор
 */
app.views.selector.hide = function () {
    app.views.elements.selector.$el.hide();
    app.views.selector.clear();
    app.views.selector.bodySelector();
    app.views.selector.is_visible = false;
}

/**
 * Размещает селектор внутри PopUp'a
 */
app.views.selector.popupSelector = function () {
    app.views.elements.selector.$el.detach();
    app.views.elements.popup.$el.append(
        app.views.elements.selector.$el
    );
}

/** 
 * Очищает селектор от данных
*/
app.views.selector.clear = function () {
    app.views.elements.selector.$menu.empty();
    app.views.elements.selector.$content.empty();
}

/**
 * Размещает селектор внутри body
 */
app.views.selector.bodySelector = function () {
    app.views.elements.selector.$el.detach();
    app.views.elements.$body.append(
        app.views.elements.selector.$el
    );
}

/**
 * Позиционирует селектор относительно инициатора
 * @param initiator
 */
app.views.selector.setPosition = function (initiator) {
    var $initiator = $(initiator),
        initiator_offset = $initiator.offset(),

        $selector = app.views.elements.selector.$el,
        selector_margin = $selector.outerHeight(true) - $selector.height();

    initiator_offset.top += $initiator.outerHeight() + selector_margin;

    app.views.elements.selector.$el.offset(initiator_offset);
}

/**
 * Рендерит рабочего в селекторе
 * @param data - данные для рендера
 */
app.views.selector.renderSelectorWorker = function (data) {
    return app.views.templates.worker_selector_el(data);
}

/**
 * Рендерит бригаду в селекторе
 * @param data - данные для рендера
 */
app.views.selector.renderSelectorBrigade = function (data) {
    return app.views.templates.brigade_selector_el(data);
}

/**
 * Рендерит заголовок меню в селекторе
 * @param data - данные для рендера
 */
app.views.selector.renderSelectorMenuEl = function (data) {
    return app.views.templates.menu_selector_el(data);
}

/**
 * Добавляет элемент меню селектора в конец
 * @param html - элемент меню
 */
app.views.selector.appendSelectorMenuEl = function (html) {
    app.views.elements.selector.$menu.append(html);
}

/**
 * Добавляет элемент элемент селектора в область контента
 * @param html - элемент меню
 */
app.views.selector.appendSelectorContentEl = function (html) {
    app.views.elements.selector.$content.append(html);
}

/**
 * Заполняет селектор данными
 * @param data - данные для заполнения
 * @param text - необязательный параметр
 */
app.views.selector.fillSelector = function (data, text) {
    // Очищаем содержимое селектора
    app.views.selector.clear();

    // Проходим по каждому разделу данных
    _.forEach(data, function (section) {

        // Рабочих сортируем по их code
        section.list = _.sortBy(section.list, ['code']);

        // Рендерим элемент меню
        var menu_el = app.views.selector.renderSelectorMenuEl(section);
        app.views.selector.appendSelectorMenuEl(menu_el);

        // Является ли набор данных активным
        if (section.is_active) {
            // Проходимся по списку элементов
            _.forEach(section.list, function (el, index) {
                var content_el;

                el.id = index; // Каждый элемент должен иметь data-id
                // Могут быть недоступные для выбора элементы
                el.disable = app.workers.hasInActiveBrigade(
                    {code: el.code}
                );

                // В зависимости от типа данных рендерим шаблон
                switch (section.id) {
                    case 0:
                        if (!app.workers.checkWorkerByText(el, text)) {
                            return;
                        }
                        content_el = app.views.selector.renderSelectorWorker(el);
                        break;
                    case 1:
                        if (!app.brigades.checkBrigadeByText(el, text)) {
                            return;
                        }
                        content_el = app.views.selector.renderSelectorBrigade(el);
                        break;
                }

                // И добавляем его в конец списка
                app.views.selector.appendSelectorContentEl(content_el);
            })
        }
    })
}

/**
 * Заново заполняет селектор
 * @param text - необязательный параметр
 */
app.views.selector.refillSelector = function (text) {
    var data = app.selector.getData();
    app.views.selector.fillSelector(data, text);
}