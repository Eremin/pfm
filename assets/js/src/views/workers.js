// Объект представлений для работников
app.views.workers = {};
// Список представлений для работников
app.views.workers.list = [];

/**
 * Рендерит работника
 * @param data - данные для рендера
 * @returns html
*/
app.views.workers.renderWorker = function (data) {
    return app.views.templates.editable_worker(data);
}

/**
 * Отображает работника в конец списка
 * @param add_el - кнопка добавления работника
 * @param html
 */
app.views.workers.appendWorker = function (add_el, html) {
    var $worker = $(html),
        $add_el = $(add_el);

    app.views.workers.list.push($worker);
    $add_el.before($worker);
}

/**
 * Очищает список представления для работников
 */
app.views.workers.clearList = function () {
    app.views.workers.list = [];
}

/**
 * Находит отрендеренных работников и добавляет их в список
 */
app.views.workers.saveRenderedWorkers = function () {
    var workers = app.views.elements.popup.$el.find(
        app.views.selectors.workers.worker
    );

    _.forEach(workers, function (worker) {
        var $worker = $(worker);

        app.views.workers.list.push($worker);
    })
}

/**
 * Удаляет представление работника
 * @param id - номер работника
 */
app.views.workers.deleteWorkerById = function (id) {
    var workers = _.remove(app.views.workers.list, function ($worker) {
        return $worker.data("id") === id;
    });

    _.forEach(workers, function ($worker) {
        $worker.remove();
    });
}

/**
 * Обновляет работника по id
 * @param id - номер работника
 * @param data - новые данные работника
 */
app.views.workers.updateWorkerById = function (data, id) {
    var workers = _.remove(app.views.workers.list, function ($worker) {
            return $worker.data("id") === id;
        }),
        $updated_worker = $(app.views.workers.renderWorker(data));

    app.views.workers.list.push($updated_worker);

    _.forEach(workers, function ($worker) {
        $worker.before($updated_worker);
        $worker.remove();
    });
}

/**
 * Вставляет бригаду на место работника с id
 * @param list - список работников (бригада)
 * @param id - номер работника, который будет удален
 */
app.views.workers.insertBrigadeWorkers = function (list, id) {
    var workers = _.remove(app.views.workers.list, function ($worker) {
            return $worker.data("id") === id;
        }),
        $worker = workers[0];
    
    _.forEach(list, function (worker) {
        var $new_worker = $(app.views.workers.renderWorker(worker));

        app.views.workers.list.push($new_worker);

        $worker.before($new_worker);
    });
    
    $worker.remove();

    // При вставке большого кол-ва работников надо производить перерасчет
    app.calculator.calculateAllAreas();
    // При вставке большого кол-ва работников надо обновить результаты карточки
    app.views.brigades.updateBrigadeCardResult(
        app.brigades.active.brigade_id
    );
    // При вставке большого кол-ва работников надо обновить бригаду
    app.views.brigades.updateBrigade(
        app.brigades.active.brigade_id
    );
}