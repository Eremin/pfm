// Объект представлений
app.views = {};

// Объект стилевых классов
app.views.css_classes = {
    scroll: {
        off: "no-scroll"
    },

    selector: {
        active: "active"
    }
}

// Объект селекторов
app.views.selectors = {
    templates: {
        area: "#area-template",
        brigade: "#brigade-template",
        brigade_card: "#brigade-card-template",
        outfit: "#outfit-template",
        worker: "#worker-template",
        editable_worker: "#editable-worker-template",
        worker_selector_el: "#worker-selector-el",
        brigade_selector_el: "#brigade-selector-el",
        menu_selector_el: "#menu-selector-el",
        brigades_results_template: "#brigades-results-template",
        area_results_template: "#area-results-template",
        brigade_card_results_template: "#brigade-card-results-template",
        results: "#results-template"
    },

    body: {
        results: ".brigades-result"
    },

    decorative_loader: {
        el: ".decorative-loader"
    },

    menu: {
        get_results: ".get-workers-results",
        clear_data: ".clear-data",
        save_data: ".save-data",
        load_data: ".load-data",
        file_upload: "#file-upload",
        date_selector_input: ".date-selector__input"
    },

    areas: {
        container: ".areas-container",
        add: ".area-add",
        delete: ".area__delete",
        results: ".area__results",
        name: ".area__name"
    },

    brigades: {
        brigade: ".js-brigade-card",
        add: ".brigades-list__el-add",
        delete: ".brigades-list__el__delete",
        results: ".brigade__results"
    },

    popup: {
        el: ".popup",
        close: ".js-close-popup"
    },

    outfits: {
        outfit: ".outfit__rows__row",
        inputs: ".outfit__rows__row__el__input",
        price: ".js-price",
        number: ".js-amount",
        minutes: ".js-time",
        total: {
            sum: ".js-sum",
            time: ".js-hours"
        },
        add: ".outfit__rows__row__el-add",
        delete: ".outfit__rows__row__el-delete"
    },

    workers: {
        worker: ".brigade__members__el",
        input: ".brigade__members__el__input",
        add: ".brigade__members__add",
        delete: ".brigade__members__el__delete"
    },

    selector: {
        el: ".selector",
        close: ".selector__close",
        menu: ".selector__menu",
        menu_el: ".selector__menu__el",
        content: ".selector__content",
        content_el: ".selector__content__el"
    }
}