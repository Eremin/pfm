// Объект представлений для нарядов
app.views.outfits = {};
// Список представлений для нарядов
app.views.outfits.list = [];

/**
 * Рендерит наряд
 * @param data - данные для рендера
 * @returns html
*/
app.views.outfits.renderOutfit = function (data) {
    return app.views.templates.outfit(data);
}

/**
 * Отображает наряд после некоторого другого наряда
 * @param html
 * @param prev_outfit_id - номер предыдущего наряда
 */
app.views.outfits.displayOutfit = function (html, prev_outfit_id) {
    var $prev_outfit = _.find(app.views.outfits.list, function ($outfit) {
            return $outfit.data("id") === prev_outfit_id;
        }),
        $outfit = $(html);

    app.views.outfits.list.push($outfit);
    $prev_outfit.after($outfit);
}

/**
 * Находит отрендеренные наряды и добавляет их в список
 */
app.views.outfits.saveRenderedOutfits = function () {
    var outfits = app.views.elements.popup.$el.find(
        app.views.selectors.outfits.outfit
    );

    _.forEach(outfits, function (outfit) {
        var $outfit = $(outfit);

        app.views.outfits.list.push($outfit);
    })
}

/**
 * Очищает список представлений для нарядов
 */
app.views.outfits.clearList = function () {
    app.views.outfits.list = [];
}

/**
 * Получаем данные из input'ов изменяемого наряда
 * @param id - номер наряда
 * @returns объект с данными из input'ов
 */
app.views.outfits.getInputData = function (id) {
    var $outfit = _.find(app.views.outfits.list, function ($outfit) {
            return $outfit.data("id") === id;
        }),
        $price = $outfit.find(app.views.selectors.outfits.price),
        $number = $outfit.find(app.views.selectors.outfits.number),
        $minutes = $outfit.find(app.views.selectors.outfits.minutes),
        data = {
            price: $price.val(),
            number: $number.val(),
            minutes: $minutes.val()
        };

    return data;
}

/**
 * Отображает результаты вычислений для наряда
 * @param outfit - наряд
 */
app.views.outfits.displayOutfitTotal = function (outfit) {
    var $outfit = _.find(app.views.outfits.list, function ($outfit) {
            return $outfit.data("id") === outfit.outfit_id;
        }),
        $total_sum = $outfit.find(app.views.selectors.outfits.total.sum),
        $total_time = $outfit.find(app.views.selectors.outfits.total.time);
        
    $total_sum.html((outfit.total.sum).toFixed(2));
    $total_time.html((outfit.total.time).toFixed(2));
}

/**
 * Удаляет представление наряда
 * @param id - номер наряда
 */
app.views.outfits.deleteOutfitById = function (id) {
    var outfits = _.remove(app.views.outfits.list, function ($outfit) {
        return $outfit.data("id") === id;
    });

    _.forEach(outfits, function ($outfit) {
        $outfit.remove();
    });
}