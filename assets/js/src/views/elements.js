// Объект элементов
app.views.elements = {
    $body: $(document.body)
};

// Шаблоны
app.views.elements.templates = {
    $area: app.views.elements.$body.find(app.views.selectors.templates.area),
    $brigade: app.views.elements.$body.find(app.views.selectors.templates.brigade),
    $brigade_card: app.views.elements.$body.find(app.views.selectors.templates.brigade_card),
    $outfit: app.views.elements.$body.find(app.views.selectors.templates.outfit),
    $worker: app.views.elements.$body.find(app.views.selectors.templates.worker),
    $editable_worker: app.views.elements.$body.find(app.views.selectors.templates.editable_worker),
    $worker_selector_el: app.views.elements.$body.find(app.views.selectors.templates.worker_selector_el),
    $brigade_selector_el: app.views.elements.$body.find(app.views.selectors.templates.brigade_selector_el),
    $menu_selector_el: app.views.elements.$body.find(app.views.selectors.templates.menu_selector_el),
    $brigades_results_template: app.views.elements.$body.find(app.views.selectors.templates.brigades_results_template),
    $area_results_template: app.views.elements.$body.find(app.views.selectors.templates.area_results_template),
    $brigade_card_results_template: app.views.elements.$body.find(app.views.selectors.templates.brigade_card_results_template),
    $results: app.views.elements.$body.find(app.views.selectors.templates.results)
};

// Декоративный анимированный загрузчик
app.views.elements.decorative_loader = {
    $el: app.views.elements.$body.find(app.views.selectors.decorative_loader.el)
};

// Меню
app.views.elements.menu = {
    $get_results: app.views.elements.$body.find(app.views.selectors.menu.get_results),
    $clear_data: app.views.elements.$body.find(app.views.selectors.menu.clear_data),
    $save_data: app.views.elements.$body.find(app.views.selectors.menu.save_data),
    $load_data: app.views.elements.$body.find(app.views.selectors.menu.load_data),
    $file_upload: app.views.elements.$body.find(app.views.selectors.menu.file_upload),
    $date_selector_input: app.views.elements.$body.find(app.views.selectors.menu.date_selector_input)
};

// Участки
app.views.elements.areas = {
    $container: app.views.elements.$body.find(app.views.selectors.areas.container)
};

// PopUp
app.views.elements.popup = {
    $el: app.views.elements.$body.find(app.views.selectors.popup.el)
};

// Селектор
app.views.elements.selector = {
    $el: app.views.elements.$body.find(app.views.selectors.selector.el),
    $close: app.views.elements.$body.find(app.views.selectors.selector.close),
    $menu: app.views.elements.$body.find(app.views.selectors.selector.menu),
    $content: app.views.elements.$body.find(app.views.selectors.selector.content)
}