// Объект представлений для popup'a
app.views.popup = {};
app.views.popup.fadeSpeed = 150;

/**
 * Показывает popup
 */
app.views.popup.showPopUp = function () {
    app.views.body.scrollOff();
    app.views.elements.popup.$el.fadeIn(
        app.views.popup.fadeSpeed
    );
}

/**
 * Скрывает и очищает popup
 */
app.views.popup.hidePopUp = function () {
    var is_active_brigade = !_.isNull(app.brigades.active);

    app.views.outfits.clearList();
    app.views.workers.clearList();
    app.views.body.scrollOn();
    app.views.elements.popup.$el.fadeOut(
        app.views.popup.fadeSpeed
    );
    app.views.popup.clearPopUp();

    // При закрытии popup'a происходит удаление пустых работников
    if (is_active_brigade) {
        app.workers.deleteEmptyWorkersFromBrigade(
            app.brigades.active
        );
    }

    // При закрытии popup'a надо производить перерасчет
    app.calculator.calculateAllAreas()

    // При закрытии popup'a надо обновить бригаду
    if (is_active_brigade) {
        app.views.brigades.updateBrigade(
            app.brigades.active.brigade_id
        );
    }

    // После закрытия popup'a убираем активную бригаду
    app.brigades.nullActiveBrigade();
}

/**
 * Очищает popup
 */
app.views.popup.clearPopUp = function () {
    app.views.elements.popup.$el.empty();
}

/**
 * Зполнят popup контентом
 * @param html - контент для заполнения
 */
app.views.popup.fillPopUp = function (html) {
    app.views.elements.popup.$el.html($(html));
}