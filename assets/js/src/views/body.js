// Объект представлений для body
app.views.body = {};

/**
 * Отключает scroll для body
 */
app.views.body.scrollOff = function () {
    app.views.elements.$body.toggleClass(
        app.views.css_classes.scroll.off, true
    );
}

/**
 * Включает scroll для body
 */
app.views.body.scrollOn = function () {
    app.views.elements.$body.toggleClass(
        app.views.css_classes.scroll.off, false
    )
}

/**
 * 
 */
app.views.body.updateResults = function () {
    var $html = $(app.views.templates.brigades_results_template(
        app.data.total
    )),
        $result = app.views.elements.$body.find(
            app.views.selectors.body.results
        );

    $result.before($html);
    $result.remove();
}