app.outfits = {}; // Объект нарядов
app.outfits.id = 1; // Номер который будет присвоен наряду
app.outfits.active = null; // Активный наряд (происходит ввод данных)

/**
 * Создает пустой наряд
 * @returns наряд
 */
app.outfits.createEmptyOutfit = function () {
    return {
        outfit_id: app.outfits.id++, // номер бригады (integer)
        price: 0, // стоимость (float)
        number: 0, // количество (float)
        minutes: 0, // минуты (float)
        total: { // итоговые
            sum: 0, // сумма и (float)
            time: 0 // время (float)
        }
    };
}

/**
 * Добавляет наряд в конец списка
 * @param brigade - бригада
 * @param outfit - наряд
 * @returns количество нарядов в бригаде
 */
app.outfits.appendOutfitToList = function (brigade, outfit) {
    return brigade.outfits.push(outfit);
}

/**
 * Вставляет наряд в определенное место списка нарядов
 * @param brigade - бригада
 * @param outfit - наряд
 * @param prev_outfit_id - номер наряда после которого надо вставить
 * @returns новый список нарядов
 */
app.outfits.insertOutfitIntoList = function (brigade, outfit, prev_outfit_id) {
    var index = _.findIndex(brigade.outfits, {outfit_id: prev_outfit_id});

    return brigade.outfits.splice(++index, 0, outfit);
}

/**
 * Возвращает наряд по его номеру
 * @param brigade - бригада
 * @param id - номер наряда
 * @returns наряд
 */
app.outfits.getOutfitById = function (brigade, id) {
    var find = false,
        query = { outfit_id: id },
        outfit = app.outfits.active;

    if (_.isNull(outfit)) {
        find = true;
    } else if (outfit.outfit_id != id) {
        find = true;
    }

    if (find) {
        outfit = _.find(brigade.outfits, query);
        app.outfits.setActiveOutfit(outfit);
    }

    return outfit;
}

/**
 * Устанавливает активный наряд
 */
app.outfits.setActiveOutfit = function (outfit) {
    app.outfits.active = outfit;
}

/**
 * Вычисляет наряд
 * @param data - данные для вычисления
 */
app.outfits.calculateOutfit = function (outfit, data) {
    data.minutes = parseFloat(data.minutes) || 0;
    data.number = parseFloat(data.number) || 0;
    data.price = parseFloat(data.price) || 0;

    outfit.minutes = Math.round(data.minutes * 10000) / 10000;
    outfit.number = Math.round(data.number * 10000) / 10000;
    outfit.price = Math.round(data.price * 10000) / 10000;

    outfit.total.sum = Math.round(outfit.price * outfit.number * 100) / 100;
    outfit.total.time = Math.round(outfit.minutes * outfit.number * 100 / 60) / 100;
}

/**
 * Удаляет наряд по его номеру
 * @param brigade - бригада
 * @param id - номер наряда
 */
app.outfits.deleteOutfitById = function (brigade, id) {
    outfits = _.remove(brigade.outfits, { outfit_id: id });
}