// Объект калькулятора
app.calculator = {}

/**
 * Вычисляет total бригады
 * @param brigade - бригада
 */
app.calculator.calculateBrigade = function (brigade) {
    var total = {
        sum: 0,
        time: 0,
        per_worker: {
            sum: 0,
            time: 0
        }
    },
        workers_length = brigade.workers.length;

    // Если в бригаде нет рабочих, то ничего не делаем
    if (workers_length > 0) {
        _.forEach(brigade.outfits, function (outfit) {
            var sum = parseFloat(outfit.total.sum),
                time = parseFloat(outfit.total.time);

            total.sum = parseFloat(total.sum) + sum;
            total.time = parseFloat(total.time) + time;
        });

        total.per_worker.sum = Math.round(total.sum * 100 / workers_length) / 100;
        total.per_worker.time = Math.round(total.time * 100 / workers_length) / 100;
    }

    brigade.total = total;
}

/**
 * Вычисляет total участка
 * @param area - участок
 */
app.calculator.calculateArea = function (area) {
    var total = {
        sum: 0,
        time: 0
    },
        brigades_length = area.brigades.length;

    // Если на участке нет бригад, то ничего не делаем
    if (brigades_length > 0) {
        _.forEach(area.brigades, function (brigade) {
            app.calculator.calculateBrigade(brigade);

            total.sum = parseFloat(total.sum) + parseFloat(brigade.total.sum);
            total.time = parseFloat(total.time) + parseFloat(brigade.total.time);
        })
    }

    area.total = total;
}

/**
 * Вычисляет все участки
 */
app.calculator.calculateAllAreas = function () {
    var total = {
        sum: 0,
        time: 0
    },
        areas_length = app.data.areas.length;

    // Если нет ни одного участка, то ничего не делаем
    if (areas_length > 0) {
        _.forEach(app.data.areas, function (area) {
            app.calculator.calculateArea(area);

            total.sum = parseFloat(total.sum) + parseFloat(area.total.sum);
            total.time = parseFloat(total.time) + parseFloat(area.total.time);
        })
    }

    app.data.total = total;
}