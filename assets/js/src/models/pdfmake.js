// Объект для работы с pdf
app.pdfmake = {};
app.pdfmake.docInfo = {
    info: {
        title: "", // Заголовок
        author: "Кирилл" // Автор
    },

    pageSize: "A4", // Размер
    pageOrientation: "landscape", // Ориентация
    pageMargins: [30, 40, 30, 30], // Отступы

    // Настройки по умолчанию
    defaultStyle: {
        fontSize: 10
    },

    footer: function (currentPage, pageCount) {
        return {
            text: currentPage.toString() + " / " + pageCount,
            alignment: "center"
        }
    }
}

/**
 * Подготавливает content для pdfmake по data
 * @param areas - данные для отрисовки в pdf
 */
app.pdfmake.makeContentByWorkersList = function (areas) {
    var content = [],
        max_worker_name_length = 0,
        max_worker_sum_length = 0,
        max_worker_time_length = 0,
        area_id = 0,
        table_template,
        table;

    // Определяем масимальные длины значений, чтобы pdf была похода на таблицу
    _.forEach(areas, function (area) {
        _.forEach(area.workers, function (worker) {
            if (worker.name.length > max_worker_name_length) {
                max_worker_name_length = worker.name.length;
            }
            if ((worker.sum).toFixed(2).length > max_worker_sum_length) {
                max_worker_sum_length = (worker.sum).toFixed(2).length;
            }
            if ((worker.time).toFixed(2).length > max_worker_time_length) {
                max_worker_time_length = (worker.time).toFixed(2).length;
            }
        })
    })

    // Шаблон таблицы
    table_template = {
        layout: 'noBorders',
        table: {
            layout: 'noBorders',
            widths: ['*', '*'],
            body: [[[]]]
        }
    }

    _.forEach(areas, function (area) {
        var colomn_id = 0;
        text = "Участок " + (area.name || "без названия") +
               " | Вал : " + (area.total.time).toFixed(2) + 
               " / " + (area.total.sum).toFixed(2) + " руб. \n";
        text = text.toUpperCase();

        area_id += 1;

        // Если номер участка не четный
        if (area_id % 2 === 1) {
            // Со готовим новую таблицу
            table = _.cloneDeep(table_template);
            colomn_id = 0;
        } else {
            // Добавляем вторую ячейку справа
            table.table.body[0].push([]);
            colomn_id = 1;
        }

        // Пишем заголовок по участку
        table.table.body[0][colomn_id].push({
            text: text,
            margin: [0, 0, 0, 5],
            bold: true
        });

        // Обрабатываем каждого работника
        _.forEach(area.workers, function (worker) {
            var text = worker.code + " | " +
                _.padEnd(worker.name, max_worker_name_length) + " | "
                + _.padEnd( (worker.sum).toFixed(2), max_worker_sum_length) + " руб."
                + " | "
                + _.padEnd( (worker.time).toFixed(2), max_worker_time_length) + " час.";

            // Записываем каждого работника в таблицу
            table.table.body[0][colomn_id].push({
                text: text,
                margin: [0, 0, 0, 0] //[0, 1, 0, 1]
            })
        });

        // Если номер участка кратен 2 или если это последний
        // участок, отправляем таблицу на отрисовку
        if ((area_id % 2 === 0) || (area_id === areas.length)) {
            content.push(table);
        }

        // Когда надо обрывать страницу
        if ((area_id % 2 === 0) && (area_id != areas.length)) {
            content.push({ text: '', pageBreak: 'after' });
        }
    });

    // Возвращаем содержимое pdf
    return content;
}

/**
 * Создает страницу pdf по контенту
 * @param content - контент страницы
 */
app.pdfmake.makePdfPageByContent = function (content) {
    var pdf_page = Object.assign({}, app.pdfmake.docInfo);

    pdf_page.header = [{
        text: "Расчет от " + app.data.date.toRusString(),
        alignment: "left",
        margin: [30, 20, 0, 0]
    }];

    pdf_page.content = content;

    return pdf_page;
}

/**
 * Создает и открывает страницу pdf в новой вкладке браузера
 * @param pdf_page - данные для создания pdf страницы
 */
app.pdfmake.downloadPdfPage = function (pdf_page) {
    var file_name = app.data.date.toRusString()  + ".pdf";

    // Моноширинные шрифты
    pdfMake.fonts = {
        Roboto: {
            normal: 'RobotoMono-Regular.ttf',
            bold: 'RobotoMono-Medium.ttf',
            italics: 'RobotoMono-Italic.ttf',
            bolditalics: 'RobotoMono-MediumItalic.ttf'
        }
    };

    // Отправляет pdf на скачивание
    pdfMake.createPdf(pdf_page).download(file_name);
}