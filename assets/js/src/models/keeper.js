// Объект хранителя
app.keeper = {}

/**
 * Сохраняет объект app.data в localStorage
 */
app.keeper.save = function () {
    var data = JSON.stringify(app.data);

    localStorage.setItem('app-data', data);
}

/**
 * Удаляет данные из localStorage
 */
app.keeper.clear = function () {
    localStorage.removeItem('app-data');
}

/**
 * Проверяет сохранены ли данные в localStorage
 */
app.keeper.isSaved = function () {
    var data = localStorage.getItem('app-data');
    return !_.isNull(data);
}

/**
 * Если данные сохранены возвращает их
 */
app.keeper.getData = function () {
    var data = localStorage.getItem('app-data');
    return JSON.parse(data);
}

/**
 * Сохраняет данные программы в JSON файл
 */
app.keeper.saveFile = function () {
    var data;
    
    if (app.keeper.isSaved()) {
        data = app.keeper.getData();
        data = JSON.stringify(data);
    }

    var element = document.createElement('a');
    element.setAttribute('href', 'data:application/json;charset=utf-8,' + encodeURIComponent(data));
    element.setAttribute('download', "pfm.json");

    element.style.display = 'none';
    document.body.appendChild(element);

    element.click();

    document.body.removeChild(element);
}