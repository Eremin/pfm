// Объект загрузчика
app.loader = {}

/**
 * Проходится по всем сущностям программы и устанавливает
 * ID'шники для новых экземпляров сущностей.
 * Это нужно в первую очередь для работников и нарядов, 
 * т.к. при загрузке я их не триггерю.
 */
app.loader.updateCurrentIDs = function () {
    var max_areas_id = 0,
        max_brigades_id = 0,
        max_workers_id = 0,
        max_outfits_id = 0;

    _.forEach(app.data.areas, function (area) {
        if (area.area_id > max_areas_id) {
            max_areas_id = area.area_id;
        };

        _.forEach(area.brigades, function (brigade) {
            if (brigade.brigade_id > max_brigades_id) {
                max_brigades_id = brigade.brigade_id;
            };

            _.forEach(brigade.workers, function (worker) {
                if (worker.worker_id > max_workers_id) {
                    max_workers_id = worker.worker_id;
                }
            });

            _.forEach(brigade.outfits, function (outfit) {
                if (outfit.outfit_id > max_outfits_id) {
                    max_outfits_id = outfit.outfit_id;
                }
            });
        });
    });

    app.areas.id = max_areas_id + 1;
    app.brigades.id = max_brigades_id + 1;
    app.workers.id = max_workers_id + 1;
    app.outfits.id = max_outfits_id + 1;
}

/**
 * Заменяет данные обной бригады, данными другой бригады
 * (это нужно, чтобы не потерялась ссылка на бригаду в app.data)
 */
app.loader.replaceBrigadeData = function (brigade, another_brigade) {
    _.forEach(brigade, function (value, key) {
        if (key != 'brigade_id') { brigade[key] = another_brigade[key] }
    });
}