app.data.areas = []; // Список участков

app.areas = {}; // Объект участков
app.areas.id = 1; // Номер который будет присвоен участку

/**
 * Создает пустой участок
 * @returns участок
 */
app.areas.createEmptyArea = function(){
    return {
        area_id: app.areas.id++, // номер участка (integer)
        name: "", // название участка (string)
        brigades: [], // список бригад (array of objects)
        total: { // итоговые
            sum: 0, // сумма и (float)
            time: 0 // время (float)
        }
    };
}

/**
 * Добавляет участок в конец списка
 * @param area - участок
 * @returns количество участков
 */
app.areas.appendAreaToList = function(area){
    return app.data.areas.push(area);
}

/**
 * Возвращает участок по его номеру
 * @param id - номер участка
 * @returns участок
 */
app.areas.getAreaById = function(id){
    return _.find(app.data.areas, {area_id: id});
}

/**
 * Удаляет участок по его номеру
 * @param id - номер участка
 * @returns удаленный участок
 */
app.areas.deleteAreaById = function(id){
    return _.remove(app.data.areas, {area_id: id});
}

/**
 * Возвращает участок по номеру бригады
 * @param id - номер бригады
 */
app.areas.getAreaByBrigadeId = function (id) {
    var area = _.find(app.data.areas, function (area) {
        return !_.isUndefined(
            _.find(area.brigades, {brigade_id: id})
        );
    })
    return area;
}

/**
 * Устанавливает название участка
 * @param area - участок
 * @param name - название участка
 */
app.areas.setAreaName = function (area, name) {
    area.name = name;
}