// Селектор
app.selector = {};
// Активные данные селектора
app.selector.data = {};

/**
 * Подготавливает данные с рабочими и бригадами
 */
app.selector.WorkersAndBrigades = function () {
    return {
        workers: {
            id: 0,
            caption: "Рабочие",
            is_active: true,
            list: app.templated_data.workers
        },
        brigades: {
            id: 1,
            caption: "Бригады",
            is_active: false,
            list: app.templated_data.brigades
        }
    }
}

/**
 * Устанавливает активные данные селектора
 * @param data - объект данных типа workers или brigades
 */
app.selector.setData = function (data) {
    app.selector.data = data;
}

/**
 * Возвращает объект активных данных селектора
 */
app.selector.getData = function () {
    return app.selector.data;
}

/**
 * Возвращает элемент списка по его index'у
 */
app.selector.getListElById = function (id) {
    var data = app.selector.getData(),
        active_data = _.find(
            data, 
            {is_active: true}
        );

    return active_data.list[id];
}

/**
 * Делает раздел селектора активным по его номеру 
 */
app.selector.setActiveSectionById = function (id) {
    var data = app.selector.getData(),
        new_active_section;

    _.forEach(data, function (section) {
        section.is_active = false;
    });

    new_active_section = _.find(data, {id: id});
    new_active_section.is_active = true;
}