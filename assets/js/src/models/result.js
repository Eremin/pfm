// Объект результата
app.result = {}
// Данные результата
app.result.data = [];

/**
 * Получение результата по некоторому участке
 * @param area - участок
 */
app.result.getResultFromArea = function (area) {
    var workers = [],
        data = {};

    // Формируем список работников, которые встретились в данном участке
    _.forEach(area.brigades, function (brigade) {
        _.forEach(brigade.workers, function (worker) {
            var saved_worker = _.find(workers, {code: worker.code});

            // Если работника нет в списке, то добавляем его туда
            if (_.isUndefined(saved_worker)) {
                // с данными из бригады, в которой он в первый раз встретился
                worker.sum = brigade.total.per_worker.sum;
                worker.time = brigade.total.per_worker.time;

                workers.push(worker);
            } else {
                // Иначе просто прибавляем sum и time
                saved_worker.sum += parseFloat(brigade.total.per_worker.sum);
                saved_worker.time += parseFloat(brigade.total.per_worker.time);
            }
        })
    });

    workers = _.sortBy(workers, "code");

    // Формируем итоговые данные по работникам данного участка
    data = {
        name: area.name,
        workers: workers,
        total: area.total
    };

    return data;
}

/**
 * Получаем результат по всем участкам
 */
app.result.getResult = function () {
    app.result.data = [];

    _.forEach(app.data.areas, function (area) {
        var area_result = app.result.getResultFromArea(area);

        app.result.data.push(area_result);
    })

    return app.result.data;
}