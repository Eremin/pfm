app.workers = {}; // Объект работников
app.workers.id = 1; // Номер который будет присвоен наряду

/**
 * Создает пустого работника
 * @returns работника
 */
app.workers.createEmptyWorker = function () {
    return {
        worker_id: app.workers.id++, // номер работника (integer)
        name: "", // Фамилия работника (string)
        code: "" // Код работника (string)
    };
}

/**
 * Добавляет работника в конец списка по номеру участка и бригады
 * @param brigade - бригада
 * @param worker - работник
 * @returns количество работников в бригаде
 */
app.workers.appendWorkerToList = function (brigade, worker) {
    return brigade.workers.push(worker);
}

/**
 * Возвращает бригаду по ее номеру и номеру участка
 * @param brigade - бригада
 * @param id - номер работника
 * @returns работника
 */
app.workers.getWorkerById = function (brigade, id) {
    return brigade.workers[--id];
}

/**
 * Удаляет работника по его номеру
 * @param brigade - бригада
 * @param id - номер работника
 * @return index
 */
app.workers.deleteWorkerById = function (brigade, id) {
    var index = _.findIndex(brigade.workers, { worker_id: id });
    _.remove(brigade.workers, { worker_id: id });
    return index;
}

/**
 * Проверяет соответствие работника текстовому запросу
 * @param worker - рабочий
 * @param request - текст для проверки
 * @returns boolean
 */
app.workers.checkWorkerByText = function (worker, request) {
    var name = _.lowerCase(worker.name),
        code = _.lowerCase(worker.code),
        result = false,
        name_index,
        code_index;

    request = _.lowerCase(request);

    name_index = name.indexOf(request);
    code_index = code.indexOf(request);

    if (_.isEmpty(request)) {
        result = true;
    } else if (~name_index || ~code_index) {
        result = true;
    }

    return result;
}

/**
 * Обновляет данные работника по номеру
 * @param new_data - новый данные работника
 * @param id - номер работника
 * @returns обновленного работника
 */
app.workers.updateWorkerById = function (new_data, id) {
    var worker = _.find(
        app.brigades.active.workers,
        { worker_id: id }
    );

    worker.name = new_data.name;
    worker.code = new_data.code;

    return worker;
}

/**
 * Вставляет список в список работников
 * @param brigade - бриагада
 * @param list - список новых рабочих
 * @param index - позиция, с которой происходит вставка
 */
app.workers.insertIntoList = function (brigade, list, index) {
    _.forEachRight(list, function (worker) {
        brigade.workers.splice(index, 0, worker);
    })
}

/**
 * Вставляет бригаду работников в список
 * @param templated_brigade - шаблон бригады
 * @param id - номер работника инициатора
 * @returns workers - список вставленных работников
 */
app.workers.insertBrigadeWorkers = function (templated_brigade, id) {
    var workers = [], // Список рабочих
        // Позиция удаленного работника
        index = app.workers.deleteWorkerById(
            app.brigades.active, id
        );

    _.forEach(templated_brigade.members, function (member) {
        var worker,
            templated_worker,
            checked_worker;

        // Пытаемся найти работника в списке
        checked_worker = _.find(
            app.brigades.active.workers, {code: member}
        );

        // Если работник с таким code уже есть
        if (!_.isUndefined(checked_worker)) {
            return; // дальше не идем
        }

        // Создаем пустого работника
        worker = app.workers.createEmptyWorker();

        // Находим работника из шаблона
        templated_worker = _.find(
            app.templated_data.workers, {code: member}
        );
        
        // Меняем основные параметры работника
        worker.name = templated_worker.name;
        worker.code = templated_worker.code;

        // Добавляем работника в локальный список
        workers.push(worker);
    });

    // Список вставляем в общий список
    app.workers.insertIntoList(
        app.brigades.active, workers, index
    )

    return workers;
}

/**
 * Проверяет наличие работника в активной бригаде
 */
app.workers.hasInActiveBrigade = function (worker) {
    var workers = _.find(app.brigades.active.workers, worker);
    return !_.isUndefined(workers);
}

/**
 * Удаляет пустых работников из бригады
 * @param brigade - бригада
 */
app.workers.deleteEmptyWorkersFromBrigade = function (brigade) {
    _.remove(brigade.workers, {code: ""});
}