app.brigades = {}; // Объект бригад
app.brigades.id = 1; // Номер который будет присвоен участку
app.brigades.active = null; // Активная бригада

/**
 * Создает пустую бригаду
 * @returns бригаду
 */
app.brigades.createEmptyBrigade = function () {
    return {
        brigade_id: app.brigades.id++, // номер бригады (integer)
        name: "", // название бригады (string)
        workers: [], // список рабочих (array of objects)
        outfits: [], // список нарядов (array of objects)
        total: { // итоговые
            sum: 0, // сумма и (float)
            time: 0, // время (float)
            per_worker: { // на одного рабочего
                sum: 0, // сумма и (float)
                time: 0 // время (float)
            },
        }
    };
}

/**
 * Добавляет бригаду в конец списка по номеру участка
 * @param area - участок
 * @param brigade - бригада
 * @returns количество бригад в участке
 */
app.brigades.appendBrigadeToList = function (area, brigade) {
    return area.brigades.push(brigade);
}

/**
 * Возвращает бригаду по ее номеру и номеру участка
 * @param area - участок
 * @param id - номер бригады
 * @returns бригада
 */
app.brigades.getBrigadeById = function (id) {
    var brigade;

    _.forEach(app.data.areas, function (area) {
        brigade = _.find(area.brigades, { brigade_id: id });

        if (!_.isUndefined(brigade)) {
            return false;
        };
    });

    return brigade;
}

/**
 * Удаляет бригаду по ее номеру
 * @param id - номер бригады
 * @returns удаленная бригада
 */
app.brigades.deleteBrigadeById = function (id) {
    var brigade;

    _.forEach(app.data.areas, function (area) {
        brigades = _.remove(area.brigades, { brigade_id: id });

        if (brigades.length) {
            return false;
        };
    });

    return brigade;
}

/**
 * Устанавливает активную бригаду
 * @param brigade - бригада
 */
app.brigades.setActiveBrigade = function (brigade) {
    app.brigades.active = brigade;
}

/**
 * Проверяет соответствие бригады текстовому запросу
 * @param brigade - бригада
 * @param request - текст для проверки
 * @returns boolean
 */
app.brigades.checkBrigadeByText = function (brigade, request) {
    var name = _.lowerCase(brigade.name),
        members = _.lowerCase(brigade.members),
        result = false,
        name_index,
        members_index;

    request = _.lowerCase(request);

    name_index = name.indexOf(request);
    members_index = members.indexOf(request);

    if (_.isEmpty(request)) {
        result = true;
    } else if (~name_index || ~members_index) {
        result = true;
    }

    return result;
}

/**
 * Делает app.brigades.active = null
 */
app.brigades.nullActiveBrigade = function () {
    app.brigades.active = null;
}