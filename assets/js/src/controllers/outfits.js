// Обработчик нажатия клавиши в input'е наряда
app.views.elements.popup.$el.on(
    "keydown",
    app.views.selectors.outfits.inputs,
    function (e) {
        var dots;

        // Если вводимый символ не число
        if ( _.isNaN(_.parseInt(e.key)) )  {
            // Если это точка или запятая
            if ((e.key === '.') || (e.key === ',')) {
                dots = this.value.match(/\./g);
                if (_.isNull(dots)) {
                    this.value += '.';
                }
            }

            if (
                (e.keyCode != 8) &&
                (e.keyCode != 9) &&
                (e.keyCode != 37) &&
                (e.keyCode != 39)
            ) {
                e.preventDefault();
            }
        }
    }
)

// Обработчик ввода в input наряда
app.views.elements.popup.$el.on(
    "input",
    app.views.selectors.outfits.inputs,
    function (e) {
        var outfit_id = _.parseInt(e.target.dataset.id),
            outfit = app.outfits.getOutfitById(app.brigades.active, outfit_id),
            input_data;

        input_data = app.views.outfits.getInputData(outfit_id);
        app.outfits.calculateOutfit(outfit, input_data);
        app.views.outfits.displayOutfitTotal(outfit);

        // При изменении наряда надо производить перерасчет
        app.calculator.calculateAllAreas();
        // При изменении наряда надо обновить результаты карточки
        app.views.brigades.updateBrigadeCardResult(
            app.brigades.active.brigade_id
        );
        // При изменении наряда надо обновить бригаду
        app.views.brigades.updateBrigade(
            app.brigades.active.brigade_id
        );

        app.keeper.save();
    }
)

// Обработчик клика по кнопке добавления наряда
app.views.elements.popup.$el.on(
    "click",
    app.views.selectors.outfits.add,
    function (e) {
        var prev_outfit_id = _.parseInt(e.target.dataset.id),
            outfit = app.outfits.createEmptyOutfit(),
            html = app.views.outfits.renderOutfit(outfit);


        app.outfits.insertOutfitIntoList(
            app.brigades.active, outfit, prev_outfit_id
        );

        app.views.outfits.displayOutfit(html, prev_outfit_id);

        app.keeper.save();
    }
)

// Обработчик клика по кнопке удаления наряда
app.views.elements.popup.$el.on(
    "click",
    app.views.selectors.outfits.delete,
    function (e) {
        var outfit_id = _.parseInt(e.target.dataset.id),
            check_deletion;

        if (app.brigades.active.outfits.length > 1) {
            check_deletion = confirm("Наряд будет удален!");
            if (check_deletion) {
                app.outfits.deleteOutfitById(app.brigades.active, outfit_id);
                app.views.outfits.deleteOutfitById(outfit_id);

                // При удалении наряда надо производить перерасчет
                app.calculator.calculateAllAreas();
                // При удалении наряда надо обновить результаты карточки
                app.views.brigades.updateBrigadeCardResult(
                    app.brigades.active.brigade_id
                );
                // При удалении наряда надо обновить бригаду
                app.views.brigades.updateBrigade(
                    app.brigades.active.brigade_id
                );

                app.keeper.save();
            }
        }
    }
)