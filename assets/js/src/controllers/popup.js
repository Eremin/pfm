// Обработчик нажатия ESC при открытом popup'e
app.views.elements.$body.on(
    "keyup",
    function (e) {
        if (e.key === "Escape") {
            if (!app.views.selector.is_visible) {
                app.views.popup.hidePopUp();

                app.keeper.save();
            }
        }
    }
)

// Обработчик клика по кнопке закрытия popup'a
app.views.elements.popup.$el.on(
    "click",
    app.views.selectors.popup.close,
    function () {
        app.views.popup.hidePopUp();

        app.keeper.save();
    }
)