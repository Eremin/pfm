app.views.elements.menu.$clear_data.on(
    "click",
    function () {
        var check_clear = confirm("Все данные будут удалены!");
        if (check_clear) {
            app.keeper.clear();
            window.location.reload();
        }
    }
);

app.views.elements.menu.$save_data.on(
    "click", 
    function () {
        if (!app.keeper.isSaved()) {
            alert("Нет данных для сохранения!");
            return;
        }
        app.keeper.saveFile();
    }
)

app.views.elements.menu.$file_upload.on(
    "change", 
    function () {
        var file = this.files[0];

        if (this.files.length > 1) {
            alert("Выберите только один файл!");
            return;
        }

        if (file) {
            var reader = new FileReader();
            reader.readAsText(file, "UTF-8");
            reader.onload = function (evt) {
                var data = evt.target.result;
                data = JSON.parse(data);
                app.data = data;
                app.keeper.save();
                window.location.reload();
            }
            reader.onerror = function (evt) {
                alert("Не удалось прочитать файл!");
            }
        }
    }
)