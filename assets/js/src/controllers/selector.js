// Обработчик нажатия ESC при открытом селекторе
app.views.elements.$body.on(
    "keyup",
    function (e) {
        if (e.key === "Escape") {
            app.views.selector.hide();

            app.keeper.save();
        }
    }
)

// Обработчик клика по селектору
app.views.elements.selector.$el.on(
    "mousedown",
    function (e) {
        e.preventDefault();
        e.stopPropagation();
    }
)

// Обработчик клика по элементу меню селектора
app.views.elements.selector.$el.on(
    "click",
    app.views.selectors.selector.menu_el,
    function (e) {
        var el_id = _.parseInt(e.target.dataset.id);

        app.selector.setActiveSectionById(el_id);
        app.views.selector.refillSelector();

        app.keeper.save();
    }
)

// Обработчик клика по кнопке закрытия селектора
app.views.elements.selector.$el.on(
    "click",
    app.views.selectors.selector.close,
    function () {
        app.views.selector.hide();

        app.keeper.save();
    }
)

// Обработчик клика по элементу селектора
app.views.elements.selector.$el.on(
    "click",
    app.views.selectors.selector.content_el,
    function (e) {
        var templated_element_id = _.parseInt(e.target.dataset.id),
            templated_element = app.selector.getListElById(
                templated_element_id
            ),
            worker_id = _.parseInt(
                app.views.selector.initiator.dataset.id
            ),
            update;

        // Если в шаблонном элементе есть code, то это работник
        if (!_.isUndefined(templated_element.code)) {
            update = app.workers.updateWorkerById(
                templated_element, worker_id
            );
            app.views.workers.updateWorkerById(
                update, worker_id
            );
        }

        // Если в шаблонном элементе есть members, то это бригада
        if (!_.isUndefined(templated_element.members)) {
            update = app.workers.insertBrigadeWorkers(
                templated_element, worker_id
            );
            app.views.workers.insertBrigadeWorkers(
                update, worker_id
            );
        }

        // При клике на элемент селектора надо производить перерасчет
        app.calculator.calculateAllAreas()

        app.views.selector.hide();
        app.keeper.save();
    }
)