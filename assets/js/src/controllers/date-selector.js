app.datepicker = 
    app.views.elements.menu.$date_selector_input.datepicker({
        onSelect(formattedDate, date) {
            if (!formattedDate.length) {
                date = new Date();
                formattedDate = date.toRusString();
                app.views.elements.menu.$date_selector_input.val(
                    formattedDate
                );
            }
    
            app.data.date = date;
            app.keeper.save();
        }
    }).data('datepicker');

