// Обработчик клика по кнопке добавления работника
app.views.elements.popup.$el.on(
    "click",
    app.views.selectors.workers.add,
    function (e) {
        var add_el = e.target,
            worker = app.workers.createEmptyWorker();

        app.workers.appendWorkerToList(app.brigades.active, worker);

        app.views.workers.appendWorker(
            add_el, app.views.workers.renderWorker(worker)
        );

        // При добавлении работника надо производить перерасчет
        app.calculator.calculateAllAreas();
        // При добавлении работника надо обновить результаты карточки
        app.views.brigades.updateBrigadeCardResult(
            app.brigades.active.brigade_id
        );
        // При добавлении работника надо обновить бригаду
        app.views.brigades.updateBrigade(
            app.brigades.active.brigade_id
        );

        app.keeper.save();
    }
)

// Обработчик клика по кнопке удаления работника
app.views.elements.popup.$el.on(
    "click",
    app.views.selectors.workers.delete,
    function (e) {
        var check_deletion = confirm("Работник будет удален!");
            worker_id = _.parseInt(e.target.dataset.id);

        if (check_deletion) {
            app.workers.deleteWorkerById(app.brigades.active, worker_id);
            app.views.workers.deleteWorkerById(worker_id);

            // При удалении работника надо производить перерасчет
            app.calculator.calculateAllAreas()
            // При удалении работника надо обновить результаты карточки
            app.views.brigades.updateBrigadeCardResult(
                app.brigades.active.brigade_id
            );
            // При удалении работника надо обновить бригаду
            app.views.brigades.updateBrigade(
                app.brigades.active.brigade_id
            );

            app.keeper.save();
        }
    }
)

// Обработчик фокусировки на элементе работника
app.views.elements.popup.$el.on(
    "focus",
    app.views.selectors.workers.input,
    function (e) {
        var data = app.selector.WorkersAndBrigades();
        app.selector.setData(data);

        app.views.selector.fillSelector(data);

        app.views.selector.popupSelector();
        app.views.selector.show(e.target);
    }
)

// Обработчик расфокусировки на элементе работника
app.views.elements.popup.$el.on(
    "blur",
    app.views.selectors.workers.input,
    function (e) {
        e.target.innerText = "";
        app.views.selector.hide();
    }
)

// Обработчик нажатия клавиши на элементе работника
app.views.elements.popup.$el.on(
    "keydown",
    app.views.selectors.workers.input,
    function (e) {
        if (
            (e.key === "Enter") || 
            (e.key === "ArrowUp") || 
            (e.key === "ArrowDown")
        ) {
            e.stopPropagation();
            e.preventDefault();
            return;
        }
    }
)

// Обработчик ввода на элементе работника
app.views.elements.popup.$el.on(
    "input",
    app.views.selectors.workers.input,
    function (e) {
        var text = e.target.innerText;

        app.views.selector.refillSelector(text);

        // Если что-то вводим, то снова показываем селектор
        app.views.selector.popupSelector();
        app.views.selector.show(e.target);
    }
)