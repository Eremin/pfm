app.views.elements.menu.$get_results.on(
    "click",
    function () {
        var result_data = app.result.getResult(),
            html = app.views.result.render(result_data),
            pdf_page = app.pdfmake.makePdfPageByContent(
                app.pdfmake.makeContentByWorkersList(result_data)
            );

        app.views.popup.fillPopUp(html);
        app.views.popup.showPopUp();
        app.pdfmake.downloadPdfPage(pdf_page);

        app.keeper.save();
    }
)