// Обработчик клика по кнопке добавления бригады
app.views.elements.$body.on(
    "click",
    app.views.selectors.brigades.add,
    function (e) {
        var area_id = _.parseInt(this.dataset.id),
            area = app.areas.getAreaById(area_id),
            brigade = app.brigades.createEmptyBrigade(),
            outfit = app.outfits.createEmptyOutfit();

        app.outfits.appendOutfitToList(brigade, outfit);

        app.brigades.appendBrigadeToList(area, brigade);
        app.views.brigades.showBrigade(
            e.target,
            app.views.brigades.renderBrigade(brigade)
        );

        app.keeper.save();
    }
);

// Обработчик клика по кнопке удаления бригады
app.views.elements.$body.on(
    "click",
    app.views.selectors.brigades.delete,
    function (e) {
        var check_deletion = confirm("Бригада будет удалена!");
            brigade_id = _.parseInt(this.dataset.id),
            area = app.areas.getAreaByBrigadeId(brigade_id);

        e.stopPropagation();

        if (check_deletion) {
            app.brigades.deleteBrigadeById(brigade_id);
            app.views.brigades.deleteBrigade(brigade_id);

            // При удалении бригады надо производить перерасчет
            app.calculator.calculateAllAreas();
            // При удалении бригады надо обновить участок
            app.views.areas.updateAreaTotalById(area.area_id);

            app.keeper.save();
        }
    }
);

// Обработчик клика по бригаде
app.views.elements.$body.on(
    "click",
    app.views.selectors.brigades.brigade,
    function (e) {
        var brigade_id = _.parseInt(this.dataset.id),
            brigade = app.brigades.getBrigadeById(brigade_id),
            brigade_card = app.views.brigades.renderBrigadeCard(brigade);

        app.brigades.setActiveBrigade(brigade);

        app.views.popup.fillPopUp(brigade_card);

        app.views.outfits.saveRenderedOutfits();
        app.views.workers.saveRenderedWorkers();

        app.views.popup.showPopUp();

        app.keeper.save();
    }
)