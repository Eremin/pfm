// Обработчик клика по кнопке добавления участка
app.views.elements.$body.on(
    "click",
    app.views.selectors.areas.add,
    function () {
        var area = app.areas.createEmptyArea();

        app.areas.appendAreaToList(area);
        app.views.areas.showArea(
            app.views.areas.renderArea(area)
        );

        app.keeper.save();
    }
);

// Обработчик клика по кнопке удаления участка
app.views.elements.$body.on(
    "click",
    app.views.selectors.areas.delete,
    function (e) {
        var check_deletion = confirm("Участок будет удален!");
            area_id = _.parseInt(this.dataset.id);

        if (check_deletion) {
            app.areas.deleteAreaById(area_id);
            app.views.areas.deleteArea(area_id);

            // При удалении участка надо производить перерасчет
            app.calculator.calculateAllAreas();
            // При удалении участка обновляем общие данные
            app.views.body.updateResults();

            app.keeper.save();
        }
    }
);

// Обработчик ввода названия участка
app.views.elements.$body.on(
    "input",
    app.views.selectors.areas.name,
    function (e) {
        var area_id = _.parseInt(e.target.dataset.id),
            area_name = e.target.innerText,
            area = app.areas.getAreaById(area_id);
        
        app.areas.setAreaName(area, area_name);

        app.keeper.save();
    }
)