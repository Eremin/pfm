// Объект приложения
var app = {};
// Данные приложения
app.data = {};
// Версия приложения
app.data.version = "18.08.2019.2";
// Итоговые данные по всем участкам
app.data.total = {
    sum: 0,
    time: 0
};

// Дата работы с программой
app.data.date = new Date();
// Метод для формирования даты в необходимом формате
app.data.date.__proto__.toRusString = function () {
    var date = this.getDate();
        month = this.getMonth() + 1;
    
    date = (date < 10) ? ("0" + date) : date;
    month = (month < 10) ? ("0" + month) : month;
    
    return date + "." + month + "." + this.getFullYear();
}