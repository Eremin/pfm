window.onload = function () {
    // Если данные сохранены
    if (app.keeper.isSaved()) {
        /**
         * Отображает сохраненные или загруженные данные
         */
        app.views.loader.load();
    }

    // Отображаем дату
    app.datepicker.selectDate(app.data.date);


    setTimeout(function () {
        app.views.elements.decorative_loader.$el.fadeOut(300);
    }, 500);   
}

